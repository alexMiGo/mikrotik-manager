## README

1. Generate a certificate with your data in the ssl-server folder using this command:
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key.pem -out server.pem
```

2. Change the sample.conf files in web, api and ssl-server with your values.
