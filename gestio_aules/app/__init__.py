from flask import Flask, Blueprint, render_template, redirect, request, flash, url_for, g, send_from_directory
import requests, os, json
#from flask_login import current_user

from functools import wraps
app = Flask(__name__)
app.secret_key = "gfrefiwbuocfr6g1456841/gtr46tr/*gtr/gtr39g4"

@app.route('/internet/static/<path:path>')
def my_static(path):
	return send_from_directory(os.path.join(app.root_path, 'static'), path)

from .config.config import readConfig
c=readConfig()
app.cfg=c.getConfig()

from .auth.Users import auth
au=auth()

bpWork = Blueprint('bpWork',__name__)

@bpWork.before_request
def profe_auth():
	devel_user=au.addDevelUser()
	g.user=devel_user
	if not g.user['role'] == 'profe' and not g.user['role'] == 'admin':
		return render_template('pages/errorValidacio.html')
	return

bpAdmin = Blueprint('bpAdmin', __name__)

@bpAdmin.before_request
def admin_auth():
	devel_user=au.addDevelUser()
	g.user=devel_user
	if not g.user['role'] == 'admin':
		return render_template('pages/noAdmin.html')
	return

from .views import admin, landing, aula_desconeguda, about, errors, logout

app.register_blueprint(bpWork, url_prefix='/internet')
app.register_blueprint(bpAdmin, url_prefix='/internet/admin')
