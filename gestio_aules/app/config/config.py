
import configparser

class readConfig(object):

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('web.conf')
        self.cfg={}
        self.cfg['ip_api'] = self.config['API']['IP']
        self.cfg['port_api'] = self.config['API']['PORT']
        self.cfg['admins'] = self.config['AUTH']['ADMINS'].split(",")
        try:
            self.cfg['devel_user']={'id':self.config.get('AUTH', 'DEVEL_USER'),
                                    'name':self.config.get('AUTH', 'DEVEL_USER'),
                                    'surname1':self.config.get('AUTH', 'DEVEL_USER'),
                                    'surname2':self.config.get('AUTH', 'DEVEL_USER'),
                                    'full_name':self.config.get('AUTH', 'DEVEL_USER'),
                                    'role': self.config.get('AUTH', 'DEVEL_ROLE'),
                                    'nif': '00000000A',
                                    'id_especialitat': 'NC',
                                    'especialitat':'Descr especialitat',
                                    'mail': 'noreplay@fake.com',
                                    'sesman_cookie': self.config.get('AUTH', 'DEVEL_USER')
                                }
        except:
            self.cfg['devel_user']=False

    def getConfig(self):
        return self.cfg
