function valida_nom_aula(nom) {
  var nomValid = new RegExp("^[^_*?'&,%#¬{:;@=()|.]+$");
  return nomValid.test(nom);
}

function valida_ip(ip) {
    return /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/i.test(ip);
}

function add_aula() {
    nom = $( "#naula" ).val();
    ip = $( "#nip" ).val();
    if (valida_nom_aula(nom) && valida_ip(ip)) {
        $( "#naula" ).css( "border-color", "green" );
        $( "#nip" ).css( "border-color", "green" );
        $.ajax({
            url: '/internet/admin/add_aula/' + nom + '/' + ip,
            type: 'POST',
            success: function(data){
              var aules = document.getElementById("daules");
              console.log('Data: '+data)
              location.reload();
            },
            error: function (xhr,status,error){
              console.log('Error al afegir aula!')
              new PNotify({
                  title: 'Error al afegir l\'aula!',
                  text: xhr.responseText,
                  type: 'error',
                  buttons: {
                      sticker: false
                  }
              });
            }
      })
    } else {
        if (!valida_nom_aula(nom)) {
            $( "#naula" ).css( "border-color", "red" );
            new PNotify({
                title: 'Nom d\'aula incorrecte',
                text: 'El nom de l\'aula no pot tenir caràcters especials',
                type: 'info'
            });
        } else {
             $( "#naula" ).css( "border-color", "green" );
        }
        if (!valida_ip(ip)) {
           $( "#nip" ).css( "border-color", "red" );
           new PNotify({
               title: 'IP incorrecta',
               type: 'info'
           });
        } else {
           $( "#nip" ).css( "border-color", "green" );
        }
    }
}

function remove_aula() {
    id = $( "#daules" ).val();
    aula_escollida = document.getElementById(id).innerHTML;
    $.ajax({
        url: '/internet/admin/remove_aula/'+ id + '/' + aula_escollida,
        type: 'POST',
        success: function(){
            console.log('Aula ' + aula_escollida + 'amb id ' + id + ' esborrada')
            location.reload();
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'ha pogut eliminar l\'aula.',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}

function new_rule() {
    interval = $( "#interval" ).val();
    dia = $( "#dia" ).val();
    aula = $( "#aula" ).val();
    $.ajax({
        url: '/internet/admin/new_rule/'+ aula + '/' + dia + '/' + interval,
        type: 'POST',
        success: function(){
            console.log("Afegida regla per l'aula " + aula + " al dia " + dia + " a l'interval " + interval)
            location.reload();
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'ha pogut afegir l\'interval',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}

function remove_rule(id,aula) {
    $.ajax({
        url: '/internet/admin/remove_rule/' + id + '/' + aula,
        type: 'POST',
        success: function(data){
            console.log(id.split('*')[1])
            $('tr#'+id.split('*')[1]).remove()
            reglas = JSON.parse(data)
            if (reglas['drops'].length == 0) {
                $('tr#'+aula).remove()
            }
            location.reload()
        },
    })
}

function remove_all_aula() {
    var table = $('#drops').DataTable();
    $('#drops').on( 'click', 'tr', function () {
        var aula = table.row( this ).id();
        $.ajax({
            url: '/internet/admin/remove_all/' + aula,
            type: 'POST',
            success: function(){
                    location.reload();
            },
        })
    } );

}

function remove_all() {
    $.ajax({
        url: '/internet/admin/remove_all',
        type: 'POST',
        success: function(){
                location.reload();
        },
    })
}

/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row

    var div = $('<div/>')
       .addClass( 'loading' )
       .text( 'Loading...' );
    $.ajax({
        url: '/internet/admin/get/drops/' + d.ip,
        data: {
            name: d.name
        },
        success: function(data) {
            drops = JSON.parse(data);
            nTable = '<table style="width:100%; text-align:center; margin: 0">';
            for (dict in drops) {
                nTable +=   '<tr><td style="width:10%;"/>'+
                            '<td style="width:30%;">'+drops[dict].time.split(',')[1]+'</td>'+
                            '<td style="width:30%;">'+drops[dict].time.split(',')[0]+'</td>'+
                            '<td style="width:30%;">'+
                                '<button onclick="remove_rule(\''+drops[dict].id+'\',\''+d.name+'\')" class="btn btn-danger btn-sm">'+
                                    '<span class="glyphicon glyphicon-remove"></span>'+
                                '</button>'+
                            '</td></tr>';
            }
            nTable += '</table>';
            div
                .html(nTable)
                .removeClass( 'loading' );
        },
    })
    return div;
}

$(document).ready(function() {
    var table = $('#drops').DataTable( {
        "dom" :'l<"#add">frtip',
        "ajax": {
            "url": '/internet/admin/get/aulesDT'
        },
        "rowId": 'name',
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data" : "name" },
            { "data" : "ip" },
            {
                "orderable" : false,
                "defaultContent" : '<div style="text-align: center"><button onclick="remove_all_aula()" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></button></div>'
            }
        ],
        "order": [[1, 'asc']]

    } );

    $('<button onclick="remove_all()" class="btn btn-default">Esborrar tots els intervals &nbsp<span class="glyphicon glyphicon-trash"></span></button>').appendTo('#add');

    // Add event listener for opening and closing details
    $('#drops tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
