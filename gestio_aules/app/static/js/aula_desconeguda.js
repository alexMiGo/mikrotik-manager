function enviar_email() {
    aula = $( "#aula" ).val();
    $.ajax({
        url: '/internet/aula_desconeguda/activacio/' + aula ,
        type: 'POST',
        success: function(){
            $('#boton_enviar').attr('disabled',true)
            new PNotify({
                title: 'Success!',
                text: 'Email enviat correctament',
                type: 'success',
                buttons: {
                    sticker: false
                }
            });
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'ha pogut enviar email',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}
