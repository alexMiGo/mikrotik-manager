function new_rule() {
    interval = $( "#interval" ).val();
    dia = $( "#dia" ).val();
    $.ajax({
        url: '/internet/landing/new_rule/' + dia + '/' + interval,
        type: 'POST',
        success: function(data){
            console.log(data)
            data = JSON.parse(data)
            console.log(data['drops'][0].id.split('*')[1])
            location.reload()
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'ha pogut afegir l\'interval',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}

function remove_rule(id) {

    $.ajax({
        url: '/internet/landing/remove_rule/' + id,
        type: 'POST',
        success: function(){
                console.log(id.split('*')[1])
                $('tr#'+id.split('*')[1]).remove()
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'ha pogut eliminar l\'interval',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}

function remove_all() {
    $.ajax({
        url: '/internet/landing/remove_all',
        type: 'POST',
        success: function(){
                location.reload();
        },
        error: function warn(){
            new PNotify({
                title: 'Error',
                text: 'No s\'han pogut eliminar l\'intervals',
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }
    })
}
