from flask import render_template
from app import app

@app.route('/about', methods=['GET'])
def about():
    return render_template('pages/about.html')
