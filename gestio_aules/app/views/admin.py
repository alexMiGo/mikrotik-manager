from flask import render_template, request, g, url_for, redirect
import json, requests, re
from app import app, bpAdmin

@bpAdmin.route('/', methods=['GET'])
def admin():
    r = requests.get('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin')
    dades = json.loads(r.text)
    dies = [{'key':'mon','value':'Dilluns'},{'key':'tue','value':'Dimarts'},{'key':'wed','value':'Dimecres'},{'key':'thu','value':'Dijous'},{'key':'fri','value':'Divendres'}]
    return render_template('pages/admin.html',dades=dades, dies=dies, user=g.user)

@bpAdmin.route('/add_aula/<nom>/<ip>', methods=['POST'])
def add_aula(nom,ip):
    nom = 'aula_' + nom
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/new_aula/' + nom + '/' + ip)
    if r.status_code == 500:
        if r.text == "The IP of this classroom already exists":
            return "L'ip de l'aula ja existeix",500,{'ContentType':'text/plain'}
        elif r.text == "The clasroom name already exists":
            return "El nom de l'aula ja existeix",500,{'ContentType':'text/plain'}
        return "No s'ha pogut afegir l'aula",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpAdmin.route('/remove_aula/<id>/<aula>', methods=['POST'])
def remove_aula(id,aula):
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/remove_aula/' + id + '/' + aula)
    if r.status_code == 500:
        return "Could not remove the aula",500,{'ContentType':'text/plain'}
    return "Deleted succesfully",200,{'ContentType':'text/plain'}

@bpAdmin.route('/new_rule/<aula>/<dia>/<interval>', methods=['POST'])
def admin_new_rule(aula,dia,interval):
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/new_rule/' + aula + '/' + dia + '/' + interval)
    if r.status_code == 500:
        return "Could not add the rule",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpAdmin.route('/remove_rule/<id>/<aula>', methods=['POST'])
def admin_remove_rule(id,aula):
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/remove_rule/' + id + '/' + aula)
    if r.status_code == 500:
        return "Could not remove the rule",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpAdmin.route('/remove_all', methods=['POST'])
@bpAdmin.route('/remove_all/<aula>', methods=['POST'])
def admin_remove_all(aula=None):
    if aula:
        r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/remove_all/' + aula)
        if r.status_code == 500:
            return "Could not remove all the rules",500,{'ContentType':'text/plain'}
        return "All rules from the class " + aula + " removed",200,{'ContentType':'text/plain'}
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/admin/remove_all')
    if r.status_code == 500:
        return "Could not remove all the rules",500,{'ContentType':'text/plain'}
    return "All rules removed",200,{'ContentType':'text/plain'}

@bpAdmin.route('/get/drops/<ip>', methods=['GET'])
def get_drops(ip):
    r = requests.get('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/get/drops/' + ip)
    if r.status_code == 500:
        return "Could not get the drops from that class",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpAdmin.route('/get/aulesDT', methods=['GET'])
def aulesDT():
    r = requests.get('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/get/aulesDT')
    if r.status_code == 500:
        return "Could not get the drops from that class",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}
