import json
import requests

from flask import render_template, request, url_for, redirect, g
from app import app, bpWork

@bpWork.route('/aula_desconeguda', methods=['GET'])
def aula_desconeguda():
    return redirect(url_for('landing'))

@bpWork.route('/aula_desconeguda/activacio/<aula>', methods=['POST'])
def activacio(aula):
    remote_addr=request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/aula_desconeguda/activacio/' + remote_addr + '/' + g.user['id'] + '/' + aula )
    if r.status_code == 500:
        return "Could not send the email",500,{'ContentType':'text/plain'}
    return "Email succesfully sent",200,{'ContentType':'text/plain'}
