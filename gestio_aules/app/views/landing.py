from flask import render_template, request, url_for, redirect, g
import json, requests
from app import app, bpWork

@bpWork.route('/', methods=['GET'])
def landing():
    remote_addr=request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
    r = requests.get('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/landing/' + remote_addr)
    if r.status_code == 500:
        return render_template('pages/aula_desconeguda.html',user=g.user)
    dades = json.loads(r.text)
    dies = [{'key':'mon','value':'Dilluns'},{'key':'tue','value':'Dimarts'},{'key':'wed','value':'Dimecres'},{'key':'thu','value':'Dijous'},{'key':'fri','value':'Divendres'}]
    return render_template('pages/landing.html', dades=dades,dies=dies, user=g.user)

@bpWork.route('/landing/new_rule/<dia>/<interval>', methods=['POST'])
def new_rule(dia,interval):
    remote_addr=request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/landing/new_rule/' + remote_addr + '/' + dia + '/' + interval)
    if r.status_code == 500:
        return "Could not add the rule",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpWork.route('/landing/remove_rule/<id>', methods=['POST'])
def remove_rule(id):
    remote_addr=request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/landing/remove_rule/' + remote_addr + '/' + id)
    if r.status_code == 500:
        return "Failed to remove the rule",500,{'ContentType':'text/plain'}
    dades = json.loads(r.text)
    return json.dumps(dades),200,{'ContentType':'application/json'}

@bpWork.route('/landing/remove_all', methods=['POST'])
def remove_all():
    remote_addr=request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
    r = requests.post('http://'+app.cfg['ip_api']+':'+app.cfg['port_api']+'/landing/remove_all/' + remote_addr)
    return "Removed all rules",200,{'ContentType':'text/plain'}
