from app import app
from flask import redirect

@app.route('/internet/logout', methods=['GET'])
def logout():
    return redirect("{}/logout".format(app.cfg['sesman_url']))
