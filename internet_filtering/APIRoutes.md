## API routes explanation:

- /get/days
```
Returns an array with the weekdays formatted.
```

- /get/hours
- /get/all/hours
```
Returns an array of dictionaries with the format:
{"key":1,"value":"8h-8h55m"}
Until 15h returns an array with hours between 8-14, afterwards between 16-22.
/get/all/hours will return all the available hours.
```

- /get/aules
- /get/aules/*ip*
```
Returns a dictionary with classrooms data:
{"aula_xxx": {"ip": "xxx.xxx.xxx.xxx/24", "disabled": false, "invalid": false, "id": "*xx"}}
If an ip is specified will only return the data from that classroom.
```

- /get/aulesDT
```
Special route used to build a Datatable.
Will return a dictionary with the classrooms with drop rules:
{"data": [{"ip": "xxx.xxx.xxx.xxx", "name": "aula_xxx"}]}
```

- /ip2aula/*ip*
```
Returns the name of a classroom. Otherwise, if the classroom doesn't exist, will return the code error 500.
```

- /get/drops
- /get/drops/*ip*
```
Returns a dictionary with all of the drop rules, with the format:
{"aula_xxx": [{"time": "8h-8h55m,mon", "disabled": false, "invalid": true, "id": "*xxx", "comment": "--drop--aula_xxx--alumnes--8h-8h55m,mon"}]}
If an ip is specified will return all the drop rules from this classroom only.
```

- /add/classroom/*ip*/*aula*
```
Adds a classroom with the specified IP and name. The name must beggin with "aula_".
If added correctly will return a code 200, otherwise an error 500. 
```
- /remove/classroom/*ip*/
```
Remove the specified classroom.
If removed correctly will return a code 200, otherwise an error 500.
```

- /add/*ip*
- /add/*ip*/*hour*
- /add/*ip*/*hour*/*day*

```
Adds an accept rule on the specified ip and adds a drop rule on the rest of the classroom.

- The hour specified has to be the value of the key field in the dictionary of get/hours or get/all/hours.
- The day specified has to be in the same format that appears in the array of get/

If only an ip is specified will return an error 500.
If an ip and an hour is specified the drop and accept rule will be added in the current day.
If the rules are added correctly will return a code 200. Otherwise an error 500.
```

- /remove/*ip*/*hour*/*day*

```
Removes a drop and accept rule.
If the rules are removed correctly will return a code 200. Otherwise an error 500.
```

- /remove_all
- /remove_all/*ip*

```
If an ip is specified removes all rules on that classroom. If not, removes all classroom rules.
If the rules are removed correctly will return a code 200. Otherwise an error 500.
```
