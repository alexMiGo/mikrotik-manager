#!/usr/bin/env python
# coding=utf-8
from flask import Flask

app = Flask(__name__,static_url_path='')

'''
App secret key for encrypting cookies
'''
app.secret_key = "\xe0\x98\xea5\xfa\x79\x93\xf4\xcf\x8f\xe2'\x1f`\x85#(\x03)\xb1\x05P\xaf\xd8"

'''
App config
'''
from .config.config import readConfig
c=readConfig()
app.cfg=c.getConfig()

'''
App logs
'''
from .config.log import log

'''
Debug should be removed on production!
'''
app.debug = True if app.cfg['log_level'] == 'DEBUG' else False
if app.debug:
    log.warning('Debug mode: {}'.format(app.debug))
else:
    log.info('Debug mode: {}'.format(app.debug))

from .filter import internetFilter
app.mktk=internetFilter()
app.mktk.getStatus()

from .api import Api

'''
Import all views
'''
from .views import Landing
from .views import Aula_desconeguda
from .views.admin import Landing
