#!/usr/bin/env python
# coding=utf-8
from flask import render_template, Response, request, redirect, url_for, stream_with_context, abort
from app import app

import json,re

from functools import wraps
from netaddr import *

import datetime
def dayOfWeek():
    weekday=['mon','tue','wed','thu','fri','sat','sun']
    return weekday[datetime.datetime.today().weekday()]

def dayArray():
    return ['mon','tue','wed','thu','fri','sat','sun']

@app.route('/get/days', methods=['GET'])
def getDays():
    return json.dumps(['mon','tue','wed','thu','fri','sat','sun']),200,{'Content-Type':'application/json'}

@app.route('/get/hours', methods=['GET'])
def getHours():
    return json.dumps(app.mktk.getArrayHours()),200,{'Content-Type':'application/json'}

@app.route('/get/all/hours', methods=['GET'])
def getAllHours():
    return json.dumps(app.mktk.getAllArrayHours()),200,{'Content-Type':'application/json'}

@app.route('/get/aules', methods=['GET'])
@app.route('/get/aules/<ip>', methods=['GET'])
def api_aules(ip=None):
    if ip is None:
        return json.dumps(app.mktk.getAules()),200,{'Content-Type':'application/json'}
    else:
        if invalid_ip(ip):
            return 'Invalid IP',500,{'Content-Type':'text/html'}
        aula=app.mktk.isIpKnown(ip)
        if aula not in list(app.mktk.getAules()):
            return 'Classroom not in the system',500,{'Content-Type':'text/html'}
        return json.dumps(app.mktk.getAules(aula)),200,{'Content-Type':'application/json'}

# Aquesta funció retorna un json formatat per funcionar amb Datatables a un HTML
@app.route('/get/aulesDT', methods=['GET'])
def api_dt_aules():
    aules = app.mktk.getAules()
    drops = app.mktk.getDrops()
    nAules = []
    for aula, dades in aules.items():
        for aulaWithDrop in drops.keys():
            if aula == aulaWithDrop:
                nAules.append({'name' : aula, 'ip' : dades['ip'].split("/")[0]})
    return json.dumps({'data' : nAules}),200,{'Content-Type':'application/json'}

@app.route('/ip2aula/<ip>', methods=['GET'])
def api_ip2aula(ip):
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    aula=app.mktk.isIpKnown(ip)
    if aula not in list(app.mktk.getAules()):
        return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    return json.dumps(aula),200,{'ContentType':'application/json'}

@app.route('/get/drops', methods=['GET'])
@app.route('/get/drops/<ip>', methods=['GET'])
def api_drops(ip=None):
    if ip is None:
        return json.dumps(app.mktk.getDrops()),200,{'Content-Type':'application/json'}
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    aula=app.mktk.isIpKnown(ip)
    if aula not in list(app.mktk.getAules()):
        return 'Classroom not in the system',500,{'ContentType':'text/html'}
    if aula in list(app.mktk.getDrops().keys()):
        return json.dumps(app.mktk.getDrops(aula)),200,{'Content-Type':'application/json'}
    return "This classroom doesn't have any drops",500,{'Content-Type':'text/html'}

@app.route('/get/accepts', methods=['GET'])
def api_accepts():
    return json.dumps(app.mktk.getAccepts()),200,{'Content-Type':'application/json'}

@app.route('/add/classroom/<ip>/<aula>', methods=['POST'])
def api_add_classroom(ip,aula):
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    if not re.match("^aula_{1}[^_ *?$%#½&=@;:|.]+$",aula, flags=0):
        return 'The clasroom name has to match aula_name',500,{'Content-Type':'text/html'}
    if app.mktk.cmdAddClassroom(aula,ip) == 'NoneName':
        return 'The clasroom name already exists',500,{'Content-Type':'text/html'}
    if app.mktk.cmdAddClassroom(aula,ip) == 'NoneIP':
        return 'The IP of this classroom already exists',500,{'Content-Type':'text/html'}
    app.mktk.apiros.talk(app.mktk.cmdAddClassroom(aula,ip))
    app.mktk.getStatus()
    aula_afegida = app.mktk.formatClassroom(aula)
    return json.dumps({aula_afegida : app.mktk.getAules(aula_afegida)}),200,{'Content-Type':'application/json'}

@app.route('/remove/classroom/<ip>', methods=['POST'])
def api_remove_classroom(ip):
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    aula=app.mktk.isIpKnown(ip)
    if aula not in list(app.mktk.getAules()):
        return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    id_aula = app.mktk.getAules(aula)['id']
    app.mktk.apiros.talk(app.mktk.cmdRemoveClassroom(id_aula))
    app.mktk.remove_rules(aula)
    app.mktk.getStatus()
    return 'Classroom ' + aula + ' deleted',200,{'Content-Type':'text/html'}

@app.route('/add/<ip>', methods=['POST'])
@app.route('/add/<ip>/<hour>', methods=['POST'])
@app.route('/add/<ip>/<hour>/<day>', methods=['POST'])
def api_add(ip,hour=None,day=None):
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    aula=app.mktk.isIpKnown(ip)
    if aula not in list(app.mktk.getAules().keys()):
        return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    if hour is None:
        return 'Interval not specified',500,{'Content-Type':'text/html'}
    if day is None:
        day=dayOfWeek()
    hourList = [hour['key'] for hour in app.mktk.getAllArrayHours()]
    if hour not in hourList:
        return 'The hour format must be the key value of: ' + str(app.mktk.getAllArrayHours()),500, {'Content-Type':'text/html'}
    if day not in dayArray():
        return 'The day format must be: ' + str(dayArray()), 500, {'Content-Type':'text/html'}
    if app.mktk.cmdIsRepeatedRule(aula,hour,day):
        return "This rule already exists",500,{'Content-Type':'text/html'}
    localapi_add_admin(aula,hour,day)
    return 'Added rule on ' + app.mktk.convertHourTime(int(hour))+','+day + ' in classroom ' + aula,200,{'Content-Type':'text/html'}

@app.route('/remove/<ip>/<hour>/<day>', methods=['POST'])
def api_remove(ip,hour,day):
    if invalid_ip(ip):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    hourList = [hour['key'] for hour in app.mktk.getAllArrayHours()]
    if hour not in hourList:
        return 'The hour format must be the key value of: ' + str(app.mktk.getAllArrayHours()),500, {'Content-Type':'text/html'}
    if day not in dayArray():
        return 'The day format must be: ' + str(dayArray()),500, {'Content-Type':'text/html'}
    aula=app.mktk.isIpKnown(ip)
    interval=app.mktk.convertHourTime(int(hour))+','+day
    if aula not in list(app.mktk.getDrops().keys()):
        return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    for dict in app.mktk.getDrops(aula):
        if dict['time'] == interval:
            localapi_remove(dict['id'],aula)
    return 'Dropped rule on ' + interval + ' in classroom ' + aula,200,{'Content-Type':'text/html'}

@app.route('/remove_all', methods=['POST'])
@app.route('/remove_all/<ip>', methods=['POST'])
def api_remove_all_drops(ip=None):
    if ip:
        if invalid_ip(ip):
            return 'Invalid IP',500,{'Content-Type':'text/html'}
        aula=app.mktk.isIpKnown(ip)
        if aula:
            for dict in app.mktk.getDrops(aula):
                localapi_remove(dict['id'],aula)
            return 'All rules in ' + aula + ' deleted',200,{'Content-Type':'text/html'}
        else:
            return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    else:
        for aula in app.mktk.getDrops():
            for dict in app.mktk.getDrops(aula):
                localapi_remove(dict['id'],aula)
        return 'All rules dropped',200,{'Content-Type':'text/html'}

def localapi_remove(id,aula):
    if aula not in list(app.mktk.getDrops().keys()):
        return 'None'
    result='None'
    for dict in app.mktk.getDrops(aula):
        if dict['id'] == id:
            teacher=app.mktk.getAccepts(comment=dict['comment'].replace('drop','accept').replace('alumnes','professor'))
            result=app.mktk.apiros.talk(app.mktk.cmdRemoveRule(id))
            if teacher is not 'None':
                for teacher in teacher:
                    result=app.mktk.apiros.talk(app.mktk.cmdRemoveRule(teacher['id']))
            else:
                print("Not found the teacher rule in:: ",dict)
    app.mktk.getStatus()
    return json.dumps(result)

def invalid_ip(ip):
    if not re.match("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$",ip , flags=0):
        return True
    return False

def localapi_add(ip_teacher,aula,hour=None,day=None):
    if aula not in list(app.mktk.getAules().keys()):
        return 'None'
    if hour is None:
        return 'None'
    if day is None:
        day=dayOfWeek()
    ip_xarxa = app.mktk.getAules(aula)['ip']
    ip_teacher2 = ip_xarxa.split('0/24')[0]+'200'
    if not IPAddress(ip_teacher) == IPAddress(ip_teacher2):
        result=app.mktk.apiros.talk(app.mktk.cmdAllowTeacher(ip_teacher2,aula,hour,day))
    result=app.mktk.apiros.talk(app.mktk.cmdAllowTeacher(ip_teacher,aula,hour,day))
    result=app.mktk.apiros.talk(app.mktk.cmdDropRule(aula,hour,day))
    app.mktk.getStatus()
    return json.dumps(result)

def localapi_add_admin(aula,hour=None,day=None):
    if aula not in list(app.mktk.getAules().keys()):
        return 'None'
    if hour is None:
        return 'None'
    if day is None:
        day=dayOfWeek()
    ip_xarxa = app.mktk.getAules(aula)['ip']
    ip_teacher = ip_xarxa[0:len(ip_xarxa) - 4] + '200'
    result=app.mktk.apiros.talk(app.mktk.cmdAllowTeacher(ip_teacher,aula,hour,day))
    result=app.mktk.apiros.talk(app.mktk.cmdDropRule(aula,hour,day))
    app.mktk.getStatus()
    return json.dumps(result)
