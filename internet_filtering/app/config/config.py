#!/usr/bin/env python
# coding=utf-8
import configparser

class readConfig(object):

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('web.conf')

        self.cfg={}
        self.cfg['log_level'] = self.config.get('LOG', 'LEVEL')
        self.cfg['log_file'] = self.config.get('LOG', 'FILE')
        self.cfg['router_ip'] = self.config.get('ROUTER', 'IP')
        self.cfg['router_port'] = self.config.get('ROUTER', 'PORT')
        self.cfg['router_user'] = self.config.get('ROUTER', 'USER')
        self.cfg['router_pass'] = self.config.get('ROUTER', 'PASS')
        self.cfg['sendmail_server'] = self.config.get('SENDMAIL', 'SERVER')
        self.cfg['sendmail_sender'] = self.config.get('SENDMAIL', 'SENDER')
        self.cfg['sendmail_receivers'] = self.config.get('SENDMAIL', 'RECEIVERS').split(",")


    def getConfig(self):
        return self.cfg
