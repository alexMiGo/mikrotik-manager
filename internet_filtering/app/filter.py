#~ from flask import Flask
import json
#~ app = Flask(__name__)

from . import app
import sys, posix, time, hashlib, binascii, socket, select
from .mktk_api import ApiRos
from netaddr import *
from .api import Api


class internetFilter:
    "Routeros api"
    def __init__(self):
        #~ self.sk = sk
        self.currenttag = 0

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((app.cfg['router_ip'], int(app.cfg['router_port'])))
        self.apiros = ApiRos(self.s);
        self.apiros.login(app.cfg['router_user'], app.cfg['router_pass']);

        self.aules={}
        self.drops={}
        self.accepts={}

    def getSocket(self):
        return self.s

    def isIpKnown(self,ip):
        for aula in self.aules:
            ip_client=IPNetwork(ip+'/24')
            ip_aula=IPNetwork(self.aules[aula]['ip'])
            if ip_client.network == ip_aula.network: return aula
        return False

    def getAules(self,aula=None):
        if aula is not None: return self.aules[aula]
        return self.aules

    def getDrops(self,aula=None):
        if aula is not None:
            if aula in list(self.drops.keys()):
                return self.drops[aula]
            else:
                return ''
        return self.drops

    def getAccepts(self,comment=None):
        if comment is not None:
            if comment in list(self.accepts.keys()):
                return self.accepts[comment]
            else:
                return ''
        return self.accepts

    def getStatus(self):
        data=['/ip/firewall/filter/print']
        rules=self.apiros.talk(data)
        self.aules={}
        self.drops={}
        self.accepts={}
        for r in rules:
            if '=jump-target' in list(r[1].keys()):
                if r[1]['=jump-target'].startswith('aula_'):
                    id=r[1]['=.id']
                    disabled=True if r[1]['=disabled']=='true' else False
                    invalid=True if r[1]['=invalid']=='true' else False
                    # Agafem nomes les no disabled (sabem que les ips son correctes)
                    if not disabled:
                        self.aules[r[1]['=jump-target']]={'id':id,'ip':r[1]['=src-address'],'disabled':disabled,'invalid':invalid}
            if '=comment' in list(r[1].keys()) and '--alumnes--' in r[1]['=comment']:
                if r[1]['=action'] == 'drop':
                    id=r[1]['=.id']
                    aula=r[1]['=chain']
                    timer=r[1]['=time']
                    disabled=True if r[1]['=disabled']=='true' else False
                    invalid=True if r[1]['=invalid']=='true' else False
                    comment=r[1]['=comment']
                    if aula not in list(self.drops.keys()): self.drops[aula]=[]
                    self.drops[aula].append({'id':id,'time':timer,'disabled':disabled,'invalid':invalid,'comment':comment})
            if '=comment' in list(r[1].keys()) and '--professor--' in r[1]['=comment']:
                if r[1]['=action'] == 'accept':
                    id=r[1]['=.id']
                    aula=r[1]['=chain']
                    timer=r[1]['=time']
                    disabled=True if r[1]['=disabled']=='true' else False
                    invalid=True if r[1]['=invalid']=='true' else False
                    comment=r[1]['=comment']
                    if comment not in list(self.accepts.keys()): self.accepts[comment]=[]
                    self.accepts[comment].append({'id':id,'time':timer,'disabled':disabled,'invalid':invalid,'aula':aula})
        return True


    def convertHourTime(self,hour):
        if hour == 1:
            return '8h-8h55m'
        if hour == 2:
            return '8h55m-9h50m'
        if hour == 3:
            return '9h50m-10h45m'
        if hour == 4:
            return '10h45m-11h15m'
        if hour == 5:
            return '11h15m-12h10m'
        if hour == 6:
            return '12h10m-13h5m'
        if hour == 7:
            return '13h5m-14h'
        if hour == 8:
            return '16h-16h55m'
        if hour == 9:
            return '16h55m-17h50m'
        if hour == 10:
            return '17h50m-18h45m'
        if hour == 11:
            return '18h45m-19h15m'
        if hour == 12:
            return '19h15m-20h10m'
        if hour == 13:
            return '20h10m-21h5m'
        if hour == 14:
            return '21h5m-22h'

    def getArrayHours(self):
        t=self.apiros.talk(self.cmdSystemClock())[0][1]['=time'].split(':')
        if int(t[0])<15:
            return [{'key':'1','value':'8h-8h55m'},
                    {'key':'2','value':'8h55m-9h50m'},
                    {'key':'3','value':'9h50m-10h45m'},
                    {'key':'4','value':'10h45m-11h15m'},
                    {'key':'5','value':'11h15m-12h10m'},
                    {'key':'6','value':'12h10m-13h5m'},
                    {'key':'7','value':'13h5m-14h'}]
        else:
            return [{'key':'8','value':'16h-16h55m'},
                    {'key':'9','value':'16h55m-17h50m'},
                    {'key':'10','value':'17h50m-18h45m'},
                    {'key':'11','value':'18h45m-19h15m'},
                    {'key':'12','value':'19h15m-20h10m'},
                    {'key':'13','value':'20h10m-21h5m'},
                    {'key':'14','value':'21h05m-22h'}]

    def getAllArrayHours(self):
            return [{'key':'1','value':'8h-8h55m'},
                    {'key':'2','value':'8h55m-9h50m'},
                    {'key':'3','value':'9h50m-10h45m'},
                    {'key':'4','value':'10h45m-11h15m'},
                    {'key':'5','value':'11h15m-12h10m'},
                    {'key':'6','value':'12h10m-13h5m'},
                    {'key':'7','value':'13h5m-14h'},
                    {'key':'8','value':'16h-16h55m'},
                    {'key':'9','value':'16h55m-17h50m'},
                    {'key':'10','value':'17h50m-18h45m'},
                    {'key':'11','value':'18h45m-19h15m'},
                    {'key':'12','value':'19h15m-20h10m'},
                    {'key':'13','value':'20h10m-21h5m'},
                    {'key':'14','value':'21h5m-22h'}]

    def cmdAllowTeacher(self, ip_teacher, aula, hour, day):
        if aula not in list(self.aules.keys()):
            return 'None'
        interval=self.convertHourTime(int(hour))+','+day
        return ['/ip/firewall/filter/add',
                '=action=accept',
                '=chain='+aula,
                '=src-address='+ip_teacher,
                '=time='+interval,
                '=comment=--accept--'+aula+'--professor--'+interval]

    def cmdDropRule(self, aula, hour, day):
        if aula not in list(self.aules.keys()):
            return 'None'
        interval=self.convertHourTime(int(hour))+','+day
        return ['/ip/firewall/filter/add',
                '=action=drop',
                '=chain='+aula,
                '=src-address='+self.aules[aula]['ip'],
                '=time='+interval,
                '=comment=--drop--'+aula+'--alumnes--'+interval]

    def cmdRemoveRule(self, id):
        return ['/ip/firewall/filter/remove',
                '=.id='+id]

    def cmdIsRepeatedRule(self,aula,hour,day):
        interval=self.convertHourTime(int(hour))+','+day
        for rule in self.getDrops(aula):
            if rule['time'] == interval:
                return True
        return False

    def remove_rules(self,aula):
        if aula in list(self.getDrops().keys()):
            del_drops = self.getDrops(aula)
            for indx in range(len(del_drops)):
                Api.localapi_remove(del_drops[indx]['id'],aula)

    def formatClassroom(self,aula):
        target = aula.lower()
        if not target.startswith('aula'):
            return 'aula_' + target
        if target.startswith('aula '):
            return target.split(' ')[0] + '_' + target.split(' ')[1]
        if not target.startswith('aula_'):
            return target[0:4] + '_' + target[4:len(target)]
        return target

    def cmdAddClassroom(self,aula,ip):
        ip_xarxa = str(IPNetwork(ip + "/24").network)
        target = self.formatClassroom(aula)
        if not self.isIpKnown(ip):
            if target not in list(self.aules.keys()):
                return ['/ip/firewall/filter/add',
                        '=action=jump',
                        '=chain=forward',
                        '=src-address='+ ip_xarxa + "/24",
                        '=connection-state=new',
                        '=jump-target='+target, # Formato : aula_loquesea
                        '=comment='+target+' - vlan' + ip_xarxa.split('.')[2]]
            return 'NoneName'
        return 'NoneIP'

    def cmdRemoveClassroom(self,id):
        return ['/ip/firewall/filter/remove',
                '=.id='+id]

    def cmdSystemClock(self):
        return ['/system/clock/print']
