from flask import render_template, redirect, request, flash, url_for
from app import app
from netaddr import *
from ..config.log import log
import json

@app.route('/aula_desconeguda/activacio/<ip>/<user>/<aula>', methods=['POST'])
def peticio_act(ip,user,aula):
    if peticio_activacio(user,aula,str(IPNetwork(ip + "/24").network)) == 'None':
        return 'Error: unable to send mail',500,{'Content-Type':'text/html'}
    return 'Successfully sent email',200,{'Content-Type':'text/html'}

def peticio_activacio(user,aula,ip):
    import smtplib
    from smtplib import SMTPException
    message = 'Subject: %s\n\n%s' % ('Activar internet a: '+ ip + '(' + aula + ')', 'Demanat per ' + user)
    try:
        smtpObj = smtplib.SMTP(app.cfg['sendmail_server'])
        smtpObj.sendmail(app.cfg['sendmail_sender'], app.cfg['sendmail_receivers'], message)
        print('Successfully sent email')
    except SMTPException:
        print('Error: unable to send email')
        return 'None'
