from flask import request
from app import app
import json
from ..api import Api

'''
App logs
'''
from ..config.log import log

@app.route('/landing/<ip>', methods=['GET'])
def landing(ip):
    aula=app.mktk.isIpKnown(ip)
    if aula:
        drops=app.mktk.getDrops(aula)
        hours=app.mktk.getArrayHours()
        return json.dumps({'drops':drops,'intervals':hours,'aula':aula}),200,{'Content-Type':'application/json'}
    return "Classroom not in the system",500,{'Content-Type':'text/html'}

@app.route('/landing/new_rule/<ip>/<dia>/<hora>', methods=['POST'])
def new_rule(ip,dia,hora):
    aula=app.mktk.isIpKnown(ip)
    if app.mktk.cmdIsRepeatedRule(aula,hora,dia):
        return "This rule already exists",500,{'Content-Type':'text/html'}
    Api.localapi_add(ip,aula,hora,dia)
    return json.dumps({'drops':app.mktk.getDrops(aula)}),200,{'Content-Type':'application/json'}

@app.route('/landing/remove_rule/<ip>/<id>', methods=['POST'])
def remove_rule(ip,id):
    aula=app.mktk.isIpKnown(ip)
    if Api.localapi_remove(id,aula) == 'None':
        return "Failed to remove the rule",500,{'Content-Type':'text/html'}
    return json.dumps({'drops':app.mktk.getDrops(aula)}),200,{'Content-Type':'application/json'}

@app.route('/landing/remove_all/<ip>', methods=['POST'])
def landing_remove_all(ip):
    aula=app.mktk.isIpKnown(ip)
    for dict in app.mktk.getDrops(aula):
        teacher=app.mktk.getAccepts(comment=dict['comment'].replace('drop','accept').replace('alumnes','professor'))
        Api.localapi_remove(dict['id'],aula)
        if teacher is 'None':
            print("Not found the teacher rule in: ",dict)
    app.mktk.getStatus()
    return 'All rules in ' + aula + ' deleted',200,{'Content-Type':'text/html'}
