from flask import render_template, Response, request, redirect, url_for
from app import app
import json,re
from netaddr import *
from ...api import Api

'''
App logs
'''
from ...config.log import log

@app.route('/admin', methods=['GET'])
def admin_landing():
    drops=app.mktk.getDrops()
    hours=app.mktk.getAllArrayHours()
    aules=app.mktk.getAules()
    return json.dumps({'aules':aules,'drops':drops,'intervals':hours}),200,{'Content-Type':'application/json'}

@app.route('/admin/new_aula/<aula>/<ip>', methods=['POST'])
def new_aula(aula,ip):
    if not re.match("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$",ip , flags=0):
        return 'Invalid IP',500,{'Content-Type':'text/html'}
    if not re.match("^aula_{1}[^_ *?$%#½&=@;:|.]+$",aula , flags=0):
        return 'The clasroom name has to match aula_name',500,{'Content-Type':'text/html'}
    if app.mktk.cmdAddClassroom(aula,ip) == 'NoneName':
        return 'The clasroom name already exists',500,{'Content-Type':'text/html'}
    if app.mktk.cmdAddClassroom(aula,ip) == 'NoneIP':
        return 'The IP of this classroom already exists',500,{'Content-Type':'text/html'}
    app.mktk.apiros.talk(app.mktk.cmdAddClassroom(aula,ip))
    app.mktk.getStatus()
    aula_afegida = app.mktk.formatClassroom(aula)
    return json.dumps({aula_afegida : app.mktk.getAules(aula_afegida)}),200,{'Content-Type':'application/json'}

@app.route('/admin/remove_aula/<id>/<aula>', methods=['POST'])
def remove_aula(id,aula):
    if aula not in list(app.mktk.getAules()):
        return 'Classroom not in the system',500,{'Content-Type':'text/html'}
    app.mktk.apiros.talk(app.mktk.cmdRemoveClassroom(id))
    app.mktk.remove_rules(aula)
    app.mktk.getStatus()
    return 'Deleted classroom ' + aula,200,{'Content-Type':'text/html'}

@app.route('/admin/new_rule/<aula>/<dia>/<hora>', methods=['POST'])
def admin_rule(aula,dia,hora):
    if app.mktk.cmdIsRepeatedRule(aula,hora,dia):
        return 'This rule already exists',500,{'Content-Type':'text/html'}
    Api.localapi_add_admin(aula,hora,dia)
    return json.dumps({'aula':aula,'dia':dia,'interval':hora}),200,{'Content-Type':'application/json'}

@app.route('/admin/remove_rule/<id>/<aula>', methods=['POST'])
def admin_remove_rule(id,aula):
    if Api.localapi_remove(id,aula) == 'None':
        return 'Failed to remove the rule',500,{'Content-Type':'text/html'}
    return json.dumps({'drops':app.mktk.getDrops(aula)}),200,{'Content-Type':'application/json'}

@app.route('/admin/remove_all', methods=['POST'])
@app.route('/admin/remove_all/<aula>', methods=['POST'])
def admin_remove_all(aula=None):
    if aula:
        if aula not in list(app.mktk.getAules()):
            return 'Classroom not in the system',500,{'Content-Type':'text/html'}
        remove_by_aula(aula)
        app.mktk.getStatus()
        return 'All rules in ' + aula + ' deleted',200,{'Content-Type':'text/html'}
    else:
        for aula in app.mktk.getDrops():
            remove_by_aula(aula)
        app.mktk.getStatus()
        return 'All rules deleted',200,{'Content-Type':'text/html'}

def remove_by_aula(aula):
    for dict in app.mktk.getDrops(aula):
        teacher=app.mktk.getAccepts(comment=dict['comment'].replace('drop','accept').replace('alumnes','professor'))
        Api.localapi_remove(dict['id'],aula)
        if teacher is 'None':
            print("Not found the teacher rule in: ",dict)
