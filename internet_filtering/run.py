#!flask/bin/python
from app import app
from gevent.wsgi import WSGIServer
from gevent import monkey; monkey.patch_all(thread=False)

if __name__ == "__main__":
	http_server = WSGIServer(('0.0.0.0', 9999), app)
	http_server.serve_forever()
